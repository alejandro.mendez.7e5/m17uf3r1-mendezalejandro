using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    [SerializeField] private GameObject pointOrigin;
    [SerializeField] private GameObject prefabBullet;
    [SerializeField] private float velocityBullet;

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.Instance.statePlayer && InputManager.Instance.GetPlayerShoot())
        {
            GameObject bullet = Instantiate(prefabBullet, pointOrigin.transform.position, pointOrigin.transform.rotation) as GameObject;
            
            Rigidbody rb = bullet.GetComponent<Rigidbody>();

            rb.AddForce(transform.forward * velocityBullet);

            Destroy(bullet, 1f);
        }
    }
}
