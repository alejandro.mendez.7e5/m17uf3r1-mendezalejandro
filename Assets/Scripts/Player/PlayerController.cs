using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    private InputManager inputManager;
    private CharacterController controller;
    private Animator animator;

    public Vector2 movementInput;
    private Vector2 movePlayer;
    private Vector3 direction;
    private Vector3 moveDirection;

    protected float movementSpeed;
    [SerializeField] private float walkSpeed;
    [SerializeField] private float runSpeed;

    [SerializeField] private float jumpForce;

    [SerializeField] private Transform cam;
    [SerializeField] private float gravity;
    protected float moveY;

    [SerializeField] private float turnSmoothTime;
    private float turnSmoothVelocity;

    private bool crouch;
    private bool interact;
    private bool dance;
    private bool jump;

    void Start()
    {
        controller = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
        inputManager = InputManager.Instance;
    }

    void Update()
    {
        if (inputManager.GetPlayerAim())
        {
            GameManager.Instance.statePlayer = false;
            animator.SetBool("Gun", GameManager.Instance.statePlayer);
            HandleJumpWithGun();
            HandleMovement();
        }
        else
        {
            GameManager.Instance.statePlayer = true;
            animator.SetBool("Gun", GameManager.Instance.statePlayer);
            HandleJump();
            HandleMovement();
            HendleCrouch();
            Dance();
            Interact();
        }
    }

    private void HandleMovement()
    {
        movePlayer = inputManager.GetPlayerMovement();
        direction = new Vector3(movePlayer.x, 0f, movePlayer.y).normalized;

        if (GameManager.Instance.statePlayer)
        {
            MovePlayerWithOutGun();
        }
        else
        {
            MovePlayerGun();
        }
       
        SetGravity();
        controller.Move(moveDirection.normalized * movementSpeed   * Time.deltaTime + Vector3.up * moveY);
    }

    private void MovePlayerWithOutGun()
    {
        if (direction.magnitude < 0.1f)
        {
            moveDirection = Vector3.zero;
            animator.SetFloat("Speed", 0f);
        }
        else
        {
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);

            HandleRun();

            moveDirection = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
        }
    }

    private void MovePlayerGun()
    {
        if (direction.magnitude < 0.1f)
        {
            moveDirection = Vector3.zero;
            animator.SetFloat("Speed", 0f);
        }
        else
        {
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);

            animator.SetFloat("Speed", walkSpeed / runSpeed);
            movementSpeed = walkSpeed / 2;

            moveDirection = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
        }
    }

    private void HandleRun()
    {
        if (inputManager.GetPlayerRun())
        {
            movementSpeed = runSpeed;
            animator.SetFloat("Speed", runSpeed / runSpeed);
        }
        else
        {
            movementSpeed = walkSpeed;
            animator.SetFloat("Speed", walkSpeed / runSpeed);
        }
    }

    private void HendleCrouch()
    {
        if (inputManager.GetPlayerCrouch() && !jump)
        {
            crouch = true;
            animator.SetBool("CrouchCheck", crouch);
            animator.SetFloat("SpeedCrouch", 0f);

            if (direction.magnitude >= 0.1f)
            {
                animator.SetFloat("SpeedCrouch", walkSpeed);
            }
        }
        else
        {
            crouch = false;
            animator.SetBool("CrouchCheck", crouch);
        }
    }

    private void Dance()
    {
        if (inputManager.GetPlayerDance())
        {
            dance = true;
            animator.SetBool("Dance", dance);
        }
        else if (movePlayer.magnitude >= 0.1f || crouch || jump || interact)
        {
            dance = false;
            animator.SetBool("Dance", dance);
        }
    }

    private void Interact()
    {
        if (inputManager.GetPlayerInteract() && direction.magnitude < 0.1f && controller.isGrounded && !dance && !crouch)
        {
            interact = true;
            animator.SetBool("Interact", interact);
        }
        else
        { 
            interact = false;
            animator.SetBool("Interact", interact);
        }

    }

    private void SetGravity()
    {
        if (!controller.isGrounded) moveY += gravity * Time.deltaTime;
    }

    private void HandleJumpWithGun()
    {
        if (controller.isGrounded && inputManager.GetPlayerJump())
        {
            jump = true;
            animator.SetBool("Jump", jump);
            animator.SetBool("Ground", jump);
            StartCoroutine(TempoJump(0.1f));
            
        }
        else
        {
            jump = false;
            animator.SetBool("Ground", jump);
            animator.SetBool("Jump", jump);
        }
    }

    private void HandleJump()
    {
        if (controller.isGrounded && inputManager.GetPlayerJump())
        {
            jump = true;
            if (direction.magnitude >= 0.1f)
            {
                animator.SetBool("JumpMove", jump);
                StartCoroutine(TempoJump(0.2f));
            }
            else
            {
                animator.SetBool("Jump", jump);
                StartCoroutine(TempoJump(0.3f));
            }
        }
        else
        {
            jump = false;
            animator.SetBool("JumpMove", jump);
            animator.SetBool("Jump", jump);

            //moveY += gravity * Time.deltaTime;
        }
    }

    IEnumerator TempoJump(float time)
    {
        yield return new WaitForSeconds(time);
        moveY = Mathf.Sqrt(jumpForce);

    }
}
