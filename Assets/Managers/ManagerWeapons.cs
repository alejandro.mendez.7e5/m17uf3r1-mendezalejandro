using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerWeapons : MonoBehaviour
{
    [SerializeField] private GameObject weapon;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.Instance.statePlayer)
        {
            weapon.SetActive(true);
        }
        else
        {
            weapon.SetActive(false);
        }
    }
}
